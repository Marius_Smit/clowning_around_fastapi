-- DROP DATABASE IF EXISTS appointments_db;

-- CREATE DATABASE appointments_db;

-- \c appointments_db;

CREATE TABLE IF NOT EXISTS Appointments (
    appointment_id SERIAL PRIMARY KEY,
    date_created TIMESTAMP DEFAULT CURRENT_DATE,
    date_of_appointment TIMESTAMP,
    client_id INTEGER,
    troupe_id INTEGER,
    status TEXT,
    rating TEXT
);

INSERT INTO Appointments(date_created, date_of_appointment, client_id, troupe_id, status, rating)
VALUES
    ('2021-01-20 14:31:11', '2021-01-30 18:00:00', 4, 1, 'upcoming', NULL),
    ('2021-01-20 08:54:06', '2021-02-18 18:00:00', 11, 2, 'upcoming', NULL),
    ('2021-01-20 14:31:11', '2021-01-01 17:00:00', 11, 1, 'completed', '\ud83e\udd21'),
    ('2021-01-21 10:30:34', '2020-01-22 18:00:00', 11, 4, 'cancelled', NULL),
    ('2021-01-21 14:13:49', '2020-01-23 18:00:00', 11, 4, 'upcoming', NULL),
    ('2021-01-21 14:14:14', '2020-01-23 19:00:00', 11, 4, 'upcoming', NULL),
    ('2021-01-26 08:20:04', '2020-01-22 19:00:21', 11, 4, 'upcoming', NULL),
    ('2021-01-26 08:20:26', '2020-01-22 20:00:00', 11, 4, 'upcoming', NULL);

CREATE TABLE IF NOT EXISTS Appointment_Issues (
    appointment_issues_id SERIAL PRIMARY KEY,
    appointment_id INTEGER REFERENCES Appointments(appointment_id),
    clown_id INTEGER,
    title TEXT,
    description TEXT
);

INSERT INTO Appointment_Issues(appointment_id, clown_id, title, description)
VALUES
    (4, 13, 'Address is too far', 'It would take me 3 hours to drive from my home to the clients specified address.'),
    (4, 13, 'Client stinks', 'Seriously. This guys smell is ruining my pazzas.');

CREATE TABLE IF NOT EXISTS Clowns_View_Client_Details (
    clowns_view_client_details_id SERIAL PRIMARY KEY,
    appointment_id INTEGER REFERENCES Appointments(appointment_id),
    clown_id INTEGER,
    reason TEXT
);

INSERT INTO Clowns_View_Client_Details(appointment_id, clown_id, reason)
VALUES
    (4, 13, 'Need to ask the client for the address.'),
    (4, 13, 'Request that the client takes a bath.');