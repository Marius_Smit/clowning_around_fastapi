from datetime import datetime
from fastapi import APIRouter, HTTPException
from typing import List

from app.api.models import AppointmentIn, AppointmentOut, AppointmentIssuesIn, \
    ClownsViewClientDetailsIn, StatusIn, RatingIn
from app.api import db_manager

appointments = APIRouter()

# note: any request bodies sent through need to have a corresponding pydantic BaseModel in
#   models.py and included in the route's method's params


@appointments.get('/clients/appointments', response_model=List[AppointmentOut])
async def get_appointments(client_id: int):
    ''' Get all appointments by client_id. URL param: client_id '''
    appointments = await db_manager.get_all_appointments_as_client(client_id)
    if not appointments or len(appointments) == 0:
        raise HTTPException(
            status_code=404, detail=f'There are no appointments for client_id {client_id}')
    return appointments


@appointments.get('/clients/appointments/upcoming', response_model=List[AppointmentOut])
async def get_upcoming_appointments(client_id: int):
    ''' Get upcoming appointments by client_id. URL param: client_id '''
    appointments = await db_manager.get_upcoming_appointments_as_client(client_id)
    if not appointments or len(appointments) == 0:
        raise HTTPException(
            status_code=404, detail=f'There are no upcoming appointments for client_id {client_id}')
    return appointments


@appointments.get('/clients/appointments/past', response_model=List[AppointmentOut])
async def get_upcoming_appointments(client_id: int):
    ''' Get past appointments by client_id. URL param: client_id '''
    appointments = await db_manager.get_past_appointments_as_client(client_id)
    if not appointments or len(appointments) == 0:
        raise HTTPException(
            status_code=404, detail=f'There are no past appointments for client_id {client_id}')
    return appointments


@appointments.get('/clients/appointments/{id}', response_model=AppointmentOut)
async def get_appointments_client(id: int):
    ''' Get single appointment by appointment_id. '''
    if id < 0:
        raise HTTPException(
            status_code=400, detail='Invalid appointment_id provided.')
    elif id != None:
        try:
            id = int(id)
            appointment = await db_manager.get_appointment(id)
            if not appointment:
                raise HTTPException(
                    status_code=404, detail=f'Appointment with id \'{id}\' does not exist.')
            return appointment
        except ValueError:
            raise HTTPException(
                status_code=400, detail='Invalid appointment_id provided.')


@appointments.put('/clients/appointments/{id}', status_code=201)
async def update_appointment_rating(id: int, payload: RatingIn):
    ''' Update appointment rating by appointment_id. Payload: rating '''
    appointment = await db_manager.get_appointment(id)
    if not appointment:
        raise HTTPException(
            status_code=404, detail=f'Appointment with id \'{id}\' does not exist.')

    update_data = payload.dict(exclude_unset=True)
    appointment_obj_from_db = AppointmentIn(**appointment)

    # validation:
    if appointment_obj_from_db.status != 'completed':
        raise HTTPException(
            status_code=400, detail='Can\'t rate on an appointment that hasn\'t been completed.')
    _temp_rating = update_data['rating'].replace(u"\U0001F921", '')
    if len(_temp_rating) != 0:
        raise HTTPException(
            status_code=400, detail='Rating may only contain clown emojis.')

    updated_appointment = appointment_obj_from_db.copy(update=update_data)
    await db_manager.update_appointment_rating(id, updated_appointment)

    return {"message": f"Successfully changed rating from {appointment_obj_from_db.rating} to {payload.rating}"}


@appointments.post('/troupeleader/createappointment', status_code=201)
async def add_appointment(payload: AppointmentIn):
    ''' Create appointment. Payload: date_of_appointment, client_id, troupe_id '''
    if payload.date_of_appointment < datetime.now():
        raise HTTPException(
            status_code=400, detail='Can not create a new appointment with a date in the past.')
    existing_appointment = await db_manager.get_check_existing_appointment_date(
        payload.date_of_appointment)
    if not existing_appointment:
        return await db_manager.insert_new_appointment(payload)
    else:
        raise HTTPException(
            status_code=400, detail='An upcoming appointment with that date already exists.')


@appointments.get('/clowns/appointments', response_model=List[AppointmentOut])
async def get_appointments(troupe_id: int):
    ''' Get all appointments by troupe_id. URL param: troupe_id '''
    appointments = await db_manager.get_all_appointments_as_clown(troupe_id)
    if not appointments or len(appointments) == 0:
        raise HTTPException(
            status_code=404, detail=f'There are no appointments for troupe_id {troupe_id}')
    return appointments


@appointments.get('/clowns/appointments/{id}', response_model=AppointmentOut)
async def get_appointments_client(id: int):
    ''' Get single appointment by appointment_id. '''
    if id < 0:
        raise HTTPException(
            status_code=400, detail='Invalid appointment_id provided.')
    try:
        id = int(id)
        appointment = await db_manager.get_appointment(id)
        if not appointment:
            raise HTTPException(
                status_code=404, detail=f'Appointment with id \'{id}\' does not exist.')
        return appointment
    except ValueError:
        raise HTTPException(
            status_code=400, detail='Invalid appointment_id provided.')


@appointments.put('/clowns/appointments/{id}')
async def update_appointment_status(id: int, payload: StatusIn):
    ''' Update appointment's status by appointment_id. Payload: status '''
    appointment = await db_manager.get_appointment(id)
    if not appointment:
        raise HTTPException(
            status_code=404, detail=f'Appointment with id \'{id}\' does not exist.')
    valid_status = ('upcoming', 'incipient', 'completed', 'cancelled')
    if payload.status not in valid_status:
        raise HTTPException(
            status_code=400, detail=f'The provided status is not valid. Valid options are: {str(valid_status)}')

    update_data = payload.dict(exclude_unset=True)
    appointment_obj_from_db = AppointmentIn(**appointment)
    updated_appointment = appointment_obj_from_db.copy(update=update_data)
    await db_manager.update_appointment_status(id, updated_appointment)

    return {"message": f"Successfully changed status from {appointment_obj_from_db.status} to {payload.status}"}


@appointments.post('/clowns/appointments/{id}/issue', status_code=201)
async def create_appointment_issue(id: int, payload: AppointmentIssuesIn):
    ''' Create a new appointment issue by appointment_id. Payload: clown_id, title, description (appointment_id optional) '''
    if id < 0:
        raise HTTPException(
            status_code=400, detail='Invalid appointment_id provided.')
    try:
        id = int(id)
        appointment = await db_manager.get_appointment(id)
        if not appointment:
            raise HTTPException(
                status_code=404, detail=f'Appointment with id \'{id}\' does not exist.')
    except ValueError:
        raise HTTPException(
            status_code=400, detail='Invalid appointment_id provided.')

    if not payload.appointment_id:
        payload.appointment_id = id

    return {"issue_id": await db_manager.insert_appointment_issue(payload)}


@appointments.post('/clowns/appointments/{id}/clientdetails', status_code=201)
async def create_request_for_client_details(id: int, payload: ClownsViewClientDetailsIn):
    ''' Create a new request for client details by appointment_id. Payload: clown_id, reason (appointment_id optional) '''
    ''' TODO: catch db exceptions/errors? '''
    if id < 0:
        raise HTTPException(
            status_code=400, detail='Invalid appointment_id provided.')
    try:
        id = int(id)
        appointment = await db_manager.get_appointment(id)
        if not appointment:
            raise HTTPException(
                status_code=404, detail=f'Appointment with id \'{id}\' does not exist.')
    except ValueError:
        raise HTTPException(
            status_code=400, detail='Invalid appointment_id provided.')

    if not payload.appointment_id:
        payload.appointment_id = id

    await db_manager.insert_client_details_request(payload)
    appointment_object_from_db = AppointmentIn(**appointment)
    return {'client_id': appointment_object_from_db.client_id}
